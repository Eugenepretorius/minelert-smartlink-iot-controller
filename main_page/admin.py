from django.contrib import admin
from .models import Settings, IO,Device
# Register your models here.

admin.site.register(IO)
admin.site.register(Settings)
admin.site.register(Device)
