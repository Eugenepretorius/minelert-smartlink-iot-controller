from django.urls import path
# from django.contrib.auth import views as auth_views
from main_page import views


urlpatterns = [
    path('', views.index, name='index'),
    # path('settings', views.settings, name='settings'),
    # path('login',  views.login_view, name='login'),
    # path('logout', auth_views.LogoutView.as_view(template_name='main_page/logout.html'), name='logout'),
    # path('gpio_update', views.gpio_update, name='gpio update'),
]
