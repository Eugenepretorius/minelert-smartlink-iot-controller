from threading import Thread
import subprocess
import socket
import time


class StateCheck(Thread):
    def __init__(self, server, interface, service_start, is_connected):
        Thread.__init__(self, name="State Check")
        self.interface = interface
        self.connected = False
        self.check_set = 0
        self.server = server
        self.service_start = service_start
        self.is_connected = is_connected

    def Check_Current_state(self):
        # print("state check", end=" => ")

        try:
            run = subprocess.Popen(['sudo', 'mmcli', '-m', '0', '|', 'grep', 'Signal'],
                                   stdin=subprocess.PIPE,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
            gsm_data, errors = run.communicate()
            run.wait()
            # print(gsm_data)
            gsm_data = str(gsm_data).split(" -------------------------")
            # print(gsm_data)

            gsm_state = gsm_data[4].split("|")[3].split(":")[1].split("'")[1]
        except IndexError:
            gsm_state = ""

        # print(gsm_state)

        if gsm_state == "connected" or gsm_state == "registered":

            self.connected = self.Check_for_network()

    def Check_for_network(self):
        check_server = self.server
        check_port = 80
        try:
            # see if we can resolve the host name -- tells us if there is
            # a DNS listening
            host = socket.gethostbyname(check_server)
            # connect to the host -- tells us if the host is actually
            # reachable
            s = socket.create_connection((host, check_port), 2)
            s.close()

            return True
        except Exception:
            # print("error", e)
            try:
                s = socket.create_connection((check_server, check_port), 2)
                s.close()
                return True
            except Exception:
                # print("error2 ", e)
                pass
        return False

    def run(self):
        while True:
            if self.connected is False:
                self.check_set = 0
                subprocess.call('nmcli con down {}'.format(str(self.interface)), shell=True)
                time.sleep(2)
                data = subprocess.call('nmcli con up {}'.format(str(self.interface)), shell=True)
                print("testing :", str(data))

                time.sleep(6)
                self.connected = self.Check_for_network()
                self.is_connected(self.connected)

            elif self.connected and self.check_set == 0:
                self.service_start(self.check_set)
                self.check_set += 1
                self.is_connected(self.connected)
