from django import forms
from main_page.models import Settings, IO


class SettingsForm(forms.ModelForm):
    class Meta:
        model = Settings
        fields = [
            'MQTT_Broker',
            'MQTT_user',
            'MQTT_password',
            'Server',
            'Server_Username',
            'Server_Password'
        ]


class IoForm(forms.ModelForm):
    class Meta:
        model = IO
        fields = [
            'IO_Type',
            'IO_Units',
            'IO_Enabled'
        ]
