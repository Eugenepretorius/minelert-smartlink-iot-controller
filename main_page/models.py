from django.db import models


# Create your models here.
class Settings(models.Model):
    MQTT_Broker = models.CharField('MQTT Broker address', max_length=100)
    MQTT_user = models.CharField('Username', max_length=50)
    MQTT_password = models.CharField('Password', max_length=40, default=None)
    MQTT_ID = models.AutoField(primary_key=True, unique=True)
    Server = models.CharField(max_length=40)
    Server_Username = models.CharField(null=True, max_length=40)
    Server_Password = models.CharField(null=True, max_length=100, )

    def __str__(self):
        return self.MQTT_Broker


class IO(models.Model):
    IO_pin = models.CharField('Pin Number ', max_length=7)
    IO_Type = models.CharField('Pin Type', max_length=50)
    IO_Name = models.CharField('Pin Name', max_length=100)
    IO_Units = models.CharField('Pin data type', max_length=40)
    IO_Enabled = models.BooleanField('Enable pin ', default=False)
    IO_ID = models.AutoField(primary_key=True, unique=True)
    IO_Digi_Export = models.CharField('optional export ', unique=True, max_length=4, null=True)

    def __str__(self):
        return self.IO_Name


class Device(models.Model):
    Customer_Name = models.CharField(max_length=100)
    Zone = models.CharField(max_length=100)
    Device_Type = models.CharField(max_length=100, null=True)
    ID = models.AutoField(primary_key=True, unique=True)

    def __str__(self):
        return self.Customer_Name


class OfflineData(models.Model):
    ID = models.AutoField(primary_key=True, unique=True)
    Date_Created = models.DateTimeField(null=True, auto_now_add=True)
    payload = models.CharField(max_length=100)
    topic = models.CharField(max_length=100)
