from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.template import loader
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from main_page.models import IO as Pins
from main_page.models import Settings, OfflineData
from main_page.serializers import OfflineSerializer
from main_page.models import Device
from main_page.forms import SettingsForm, IoForm
import paho.mqtt.client as mqtt
from django.contrib import messages
from main_page.IO import IOManager
from main_page.Statecheck import StateCheck
from django.shortcuts import render
import subprocess
import time
import json
from threading import Thread
from django.contrib.auth.forms import AuthenticationForm


mqttc = mqtt.Client("")

pin_list = {}
last_val = {}
server_user, Server_Password, user = "", "", ""
server = ""
ID = ""
password = None
broker = None
mqtt_connection = False
Token = ""


def on_connect(client, userdata, flags, rc):
    global mqtt_connection
    print("Connected with result code "+str(rc))
    mqtt_connection = True
    Customer_Name = ""
    for device in Device.objects.all():
        Customer_Name = device.Customer_Name

    state_topic = "mcs/" + Customer_Name + "/" + ID + "/device/connection_state"
    will_payload = json.dumps({"State": "Offline"})
    Birth_payload = json.dumps({"State": "Online"})

    mqttc.will_set(topic=state_topic, payload=will_payload, retain=True)
    mqttc.publish(topic=state_topic, payload=Birth_payload, retain=True)

    status_led('2,4', 1)


def on_disconnect(client, userdata, rc):
    global mqtt_connection
    if rc != 0:
        status_led('2,4', 0)
        mqtt_connection = False
        mqttc.reconnect()


def index(request):
    global mqtt_connection, server
    Broker = ""
    for data in Settings.objects.all():
        server = str(data.Server)
        Broker = str(data.MQTT_Broker)

    try:
        run = subprocess.Popen(['sudo', 'mmcli', '-m', '0', '|', 'grep', 'Signal'],
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
        gsm_data, errors = run.communicate()
        run.wait()
        # print(gsm_data)
        gsm_data = str(gsm_data).split(" -------------------------")
        # print(gsm_data)

        gsm_state = gsm_data[4].split("|")[3].split(":")[1].split("'")[1]
        gsm_Signal = gsm_data[4].split("|")[6].split(":")[1].split("(")[0].replace("'", "")
        gsm_imei = gsm_data[8].split("|")[1].split(":")[1].split("'")[1]
        gsm_number = gsm_data[3].split("|")[1].split(":")[1].split("'")[1]
        gsm_operator = gsm_data[8].split("|")[4].split(":")[1].split("'")[1]
        gsm_data = gsm_data[1].split("|")[7].split(",")[1].split("'")[0].upper()
    except IndexError:
        gsm_state = ""
        gsm_Signal = ""
        gsm_imei = ""
        gsm_number = ""
        gsm_operator = ""
        gsm_data = ""

    if mqtt_connection:
        mqtt_state = 'connected'
    else:
        mqtt_state = 'disconnected'

    if is_connected(server):
        mcs_server = "connected"

    else:
        mcs_server = "disconnected"

    context = {'Signal': gsm_Signal,
               'imei': gsm_imei,
               "sim_number": gsm_number,
               "Operator": gsm_operator,
               "Data_type": gsm_data,
               "State": gsm_state,
               "Server": server,
               "Server_state": mcs_server,
               "MQTT_Broker": Broker,
               "MQTT_State": mqtt_state}

    template = loader.get_template('main_page/index.html')
    return HttpResponse(template.render(context, request))


@login_required(login_url='/login')
@csrf_protect
def settings(request):
    ID = 0
    i = 8
    IO = {}
    settings_MQTT_Broker = "localhost"
    settings_MQTT_user = ""
    settings_MQTT_password = ""
    settings_Server = "localhost"
    settings_Server_Username = ""
    settings_Server_Password = ""

    for setting in Settings.objects.all():
        settings_MQTT_Broker = str(setting.MQTT_Broker)
        settings_MQTT_user = str(setting.MQTT_user)
        settings_MQTT_password = str(setting.MQTT_password)
        settings_Server = str(setting.Server)
        settings_Server_Username = str(setting.Server_Username)
        settings_Server_Password = str(setting.Server_Password)
        ID = int(setting.MQTT_ID)

    for io in Pins.objects.all():
        IO_Type = str(io.IO_Type)
        IO_Units = str(io.IO_Units)
        IO_Enabled = io.IO_Enabled
        # print(IO_Enabled, io, sep=":", end="**")
        if IO_Enabled:
            enabled = "checked"
        else:
            enabled = ""
        IO[i] = {'IO_Type': IO_Type, 'IO_Units': IO_Units, 'IO_Enabled': enabled}
        i -= 1

    # print('IOOOOOOOOOOOOO ', IO)

    context = {
        "MQTT_Broker": settings_MQTT_Broker,
        "MQTT_user": settings_MQTT_user,
        "MQTT_password": settings_MQTT_password,
        "Server": settings_Server,
        "Server_Username": settings_Server_Username,
        "Server_Password": settings_Server_Password,

        "IO_Type1": IO[1]['IO_Type'],
        "IO_Type2": IO[2]['IO_Type'],
        "IO_Type3": IO[3]['IO_Type'],
        "IO_Type4": IO[4]['IO_Type'],
        "IO_Type5": IO[5]['IO_Type'],
        "IO_Type6": IO[6]['IO_Type'],
        "IO_Type7": IO[7]['IO_Type'],
        "IO_Type8": IO[8]['IO_Type'],

        "IO_Units1": IO[1]['IO_Units'],
        "IO_Units2": IO[2]['IO_Units'],
        "IO_Units3": IO[3]['IO_Units'],
        "IO_Units4": IO[4]['IO_Units'],
        "IO_Units5": IO[5]['IO_Units'],
        "IO_Units6": IO[6]['IO_Units'],
        "IO_Units7": IO[7]['IO_Units'],
        "IO_Units8": IO[8]['IO_Units'],

        "IO_Enabled1": IO[1]['IO_Enabled'],
        "IO_Enabled2": IO[2]['IO_Enabled'],
        "IO_Enabled3": IO[3]['IO_Enabled'],
        "IO_Enabled4": IO[4]['IO_Enabled'],
        "IO_Enabled5": IO[5]['IO_Enabled'],
        "IO_Enabled6": IO[6]['IO_Enabled'],
        "IO_Enabled7": IO[7]['IO_Enabled'],
        "IO_Enabled8": IO[8]['IO_Enabled'],

    }

    if request.method == 'POST':
        print("post")
        article = Settings.objects.get(MQTT_ID=ID)
        form = SettingsForm(request.POST, instance=article)
        # check whether it's valid:
        print("post")
        if form.is_valid():
            print('valid')
            form.save()
            return render(request, 'main_page/settings.html', context)
        return render(request, 'main_page/settings.html', context)
    else:
        print("get")

        return render(request, 'main_page/settings.html', context)


@csrf_protect
def gpio_update(request):
    errors = {}
    if request.method == 'POST':
        data_amount = 8
        i = 1
        while i <= data_amount:
            enabled = request.POST.get('IO_Enabled'+str(i))
            IO_Type = request.POST.get('IO_Type'+str(i))
            IO_Units = request.POST.get('IO_Units'+str(i))
            pin_form = {
                'IO_Type': str(IO_Type),
                'IO_Units': str(IO_Units),
                'IO_Enabled': enabled
            }
            pin = Pins.objects.get(IO_pin=int(i))
            form = IoForm(pin_form, instance=pin)
            if form.is_valid():
                form.save()
                # print('valid GPIO')

            else:
                # print("invalid GPIO")
                errors = {"ERRORS": "GPIO error"}

            i += 1

        return HttpResponseRedirect('/settings', errors)


def setup():
    global user, password, broker, server, Token, ID, server_user, Server_Password
    status_led('2,3', 0)
    for data in Settings.objects.all():
        user = str(data.MQTT_user)
        password = str(data.MQTT_password)
        broker = str(data.MQTT_Broker)
        server = str(data.Server)
        server_user = str(data.Server_Username)
        Server_Password = str(data.Server_Password)
        break

    # print("broker", broker)

    run = subprocess.Popen(['sudo', 'mmcli', '-m', '0', '|', 'grep', 'Signal'],
                           stdin=subprocess.PIPE,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)
    gsm_data, errors = run.communicate()
    run.wait()
    # print(gsm_data)
    gsm_data = str(gsm_data).split(" -------------------------")
    try:
        ID = gsm_data[8].split("|")[1].split(":")[1].split("'")[1]
    except IndexError:
        ID = 'error'

    status_led('2,3', 1)

    io = IOManager(Pins, return_val)
    io.start()

    State = StateCheck(broker, 'cellular', state_run, is_connected)
    State.start()


def GetData(part1, part2, part3, part4, part5, part6, part7):
    run = subprocess.Popen([part1, part2, part3, part4, part5, part6, part7],
                           stdin=subprocess.PIPE,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)
    output, errors = run.communicate()
    run.wait()

    return output


"""
def Get_Token(server, server_username, server_password):
    try:
        urllib.request.urlopen('http://'+server+'/admin')  # Python 3.x

        token = True
        print('token true')
    except Exception as e:
        print(e)
        token = False

    if token:
        Token_request = requests.post(url='http://'+server+'/data/login', data={'username': server_username,
                                                                                'password': server_password})
        return_data = Token_request.json()
        # print("test :", return_data)
        # Checker(OfflineData)
        token = False
        try:
            return return_data['token']
        except KeyError:
            pass
    else:
        return 'offline'
"""


def return_val(topic, payload, Customer_Name):

    print("broker :", broker, "customer", Customer_Name)
    # Token = Get_Token(server=server, server_username=server_user, server_password=Server_Password)

    if Token == 'offline':
        OfflineData.payload = payload
        OfflineData.topic = topic
        data = {'topic': topic, 'payload': payload}
        serializer = OfflineSerializer(data=data)
        if serializer.is_valid():
            serializer.save()

    else:
        # post_data = requests.post(url='http://'+server+'/data/data', headers={'Authorization': 'Token '+Token}, data={
        # 'Device_ID': ID,
        # 'Topic': topic,
        # 'payload': payload
        # })

        # return_value = post_data.json()
        Checker(OfflineData)
        mqttc.publish(topic=topic, payload=payload)


def state_run(count):
    global mqtt_connection, Token
    # print("service starting")

    # MQTT
    if mqtt_connection is False:
        try:
            mqttc.username_pw_set(username=user, password=password)
            mqttc.loop_start()
            mqttc.connect(broker)
        except Exception as e:
            # print("State Error :", e)
            if e == "NetworkManager is not running":
                subprocess.call("sudo reboot -n", shell=True)
    time.sleep(2)

    if int(count) == 0:
        count += 1
        diagnostic.start()
    #    Token = Get_Token(server=server, server_username=server_user, server_password=Server_Password)


def is_connected(connection):
    return connection


def status_led(led, status):
    port = led.split(",")[0]
    pin = led.split(",")[1]
    answer = str((int(port) - 1) * 32 + int(pin))
    subprocess.call('echo {} > /sys/class/gpio/export'.format(str(answer)), shell=True)
    subprocess.call('echo out > /sys/class/gpio/gpio{}/direction'.format(str(answer)), shell=True)
    subprocess.call('echo {} > /sys/class/gpio/gpio{}/value'.format(str(status), str(answer)), shell=True)


@csrf_protect
def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.info(request, "You are now logged ")
                return HttpResponseRedirect('/settings')
            else:
                messages.error(request, "Invalid username or password.")
        else:
            messages.error(request, "Invalid username or password.")
    form = AuthenticationForm()
    return render(request=request,
                  template_name="main_page/login.html",
                  context={"form": form})


def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/')


class Checker:
    def __init__(self, check_data):

        self.data = check_data
        self.offline_data = {}
        self.run = True
        self.i = 0

        self.check_for_data()

    def check_for_data(self):

        try:
            for offline_data in self.data.objects.all():
                self.offline_data[self.i] = offline_data

                self.i += 1

        except Exception as e:
            print(e)

        self.run_upload()

    def run_upload(self):
        if self.i >= 1:
            for offline_data in self.data.objects.all():
                key = offline_data.ID
                topic = offline_data.topic
                payload = offline_data.payload
                # date = offline_data.Date_Created
                # data = {
                #    'Device_ID': ID,
                #    'Topic': topic,
                #    'payload': payload+{"Date_created": date}
                #   }
                # print(key)

                mqttc.publish(topic=topic, payload=payload)
                self.data.objects.filter(pk=int(key)).delete()

            self.i = 0


def signal_update():
    next_send = 0
    Customer_Name = ""
    for device in Device.objects.all():
        Customer_Name = device.Customer_Name

    topic = "mcs/" + Customer_Name + "/" + ID + "/device/diagnostic"

    while True:
        if next_send == 0:
            run = subprocess.Popen(['sudo', 'mmcli', '-m', '0', '|', 'grep', 'Signal'],
                                   stdin=subprocess.PIPE,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
            gsm_data, errors = run.communicate()
            run.wait()
            # print(gsm_data)
            gsm_data = str(gsm_data).split(" -------------------------")
            try:
                gsm_signal = int(gsm_data[4].split("|")[6].split(":")[1].split("(")[0].replace("'", ""))
                operator = gsm_data[8].split("|")[4].split(":")[1].split("'")[1]
                data = json.dumps({"signal": gsm_signal, "operator": operator})
                print("starting")
                mqttc.publish(topic=topic, payload=str(data))

                next_send = 18000  # 4 hours
            except IndexError:
                pass

            except ValueError:
                pass

        else:
            next_send -= 1
            # print("time till next update :", next_send)

        time.sleep(1)


diagnostic = Thread(target=signal_update)
mqttc.on_connect = on_connect
mqttc.on_disconnect = on_disconnect
setup()
