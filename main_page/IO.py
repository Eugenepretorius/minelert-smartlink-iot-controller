from threading import Thread
from main_page.models import Device
import subprocess
import json
import os


class IOManager(Thread):
    def __init__(self, pins, send):
        Thread.__init__(self, name="IO Manager")
        self.Pins = pins
        self.change_check = True
        self.pin_list = {}
        self.last_val = {}
        self.send = send
        self.i = 0
        self.pin_list_len = 0

        # pin properties place holder
        self.ID = ""
        self.pin = ""
        self.enabled = "False"
        self.units = ""
        self.Device_type = ""
        self.Device_name = ""
        self.setup = True
        self.Customer_Name = ""
        self.topic = "/topic"
        self.topic2 = "mcs"
        self.operator = ""
        self.get_info()

    def get_info(self):
        run = subprocess.Popen(['sudo', 'mmcli', '-m', '0', '|', 'grep', 'Signal'],
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
        gsm_data, errors = run.communicate()
        run.wait()
        gsm_data = str(gsm_data).split(" -------------------------")
        try:
            ID = gsm_data[8].split("|")[1].split(":")[1].split("'")[1]
        except IndexError:
            ID = 'error'

        self.ID = ID
        self.read_pin()

    def read_pin(self):
        i = self.i
        i += len(self.Pins.objects.all())
        # print(values.IO_ID)
        self.pin_list_len += len(self.Pins.objects.all())

        for export_pins in self.Pins.objects.all():
            # print('index ', i, "export value", export_pins.IO_Digi_Export)
            port = str(export_pins.IO_Digi_Export).split(",")[0]
            pin = str(export_pins.IO_Digi_Export).split(",")[1]

            answer = str((int(port) - 1) * 32 + int(pin))
            path = '/sys/class/gpio/gpio' + answer
            if os.path.isdir(path) is False:
                os.system(
                    'echo ' + answer + ' > /sys/class/gpio/export; echo in > /sys/class/gpio/gpio' +
                    answer + '/direction'
                )

            run = subprocess.Popen(['cat', '/sys/class/gpio/gpio' + answer + '/value'],
                                   stdin=subprocess.PIPE,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
            pinvalue, errors = run.communicate()
            pinvalue = pinvalue.decode('utf-8')
            # print('pinvalue ', pinvalue)
            self.pin_list["pin" + str(i)] = str(answer) + "," +\
                                            str(export_pins.IO_Enabled) + "," + \
                                            str(export_pins.IO_Units) + "," +\
                                            str(pinvalue) + "," + \
                                            str(export_pins.IO_Type) + "," +\
                                            str(export_pins.IO_Name)

            if self.setup is True:
                # print("setup pin ", i)
                self.last_val["pin" + str(i)] = pinvalue

                for device in Device.objects.all():
                    self.Customer_Name = device.Customer_Name

                self.topic = 'mcs/' + self.Customer_Name + "/" + self.ID + "/device/event"

            i -= 1
        self.setup = False

    def get_pin_data(self):
        for data in self.pin_list:
            self.pin = str(self.pin_list[data]).split(",")[3].replace("(", "")
            self.enabled = str(self.pin_list[data]).split(",")[1]
            self.units = str(self.pin_list[data]).split(",")[2]
            self.Device_type = str(self.pin_list[data]).split(",")[4]
            self.Device_name = str(self.pin_list[data]).split(",")[5]

    def stop(self):
        print("stopping io")
        self.change_check = False

    def run(self):
        while self.change_check:
            for data in self.pin_list:
                # print("pin ", data, " current pin", str(self.pin_list[data]).split(",")[3].replace("(", ""),
                # " last value", self.last_val[data])
                if str(self.pin_list[data]).split(",")[1] == "True" and\
                        int(str(self.pin_list[data]).split(",")[3].replace("(", ""))\
                        != int(self.last_val[data]):
                    # print("pin change", self.pin, data, self.last_val[data], sep=":")
                    self.last_val[data] = int(str(self.pin_list[data]).split(",")[3].replace("(", ""))

                    if int(str(self.pin_list[data]).split(",")[3].replace("(", "")) == 1:
                        pin_value = True
                    else:
                        pin_value = False
                    payload = json.dumps(
                        {
                            'id': int(str(data).split('pin')[1]),
                            'value': pin_value,
                        }
                    )
                    # topic = str(self.pin_list[data]).split(",")[4]
                    # topic2 = str(self.pin_list[data]).split(",")[2]
                    self.send(self.topic, payload, self.Customer_Name)
            self.read_pin()
