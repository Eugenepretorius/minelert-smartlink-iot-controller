from main_page.models import OfflineData
from rest_framework import serializers


class OfflineSerializer(serializers.ModelSerializer):
    class Meta:
        model = OfflineData
        fields = ['topic', 'payload']
